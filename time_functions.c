/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   time_functions.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 21:19:25 by aboucher          #+#    #+#             */
/*   Updated: 2016/03/05 21:19:28 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

t_env				*get_env(t_env *e)
{
	static t_env	*tmp = NULL;

	if (e)
		tmp = e;
	return (tmp);
}

char				*sec_to_min(int n)
{
	char			*str;

	str = ft_strcat(ft_itoa((n / 60) % 60), ":");
	if (n % 60 < 10)
		str = ft_strcat(ft_strcat(str, "0"), ft_itoa(n % 60));
	else
		str = ft_strcat(str, ft_itoa(n % 60));
	return (str);
}

void				*timer(void *arg)
{
	static int		time = 0;
	char			*str;
	t_env			*e;

	(void)arg;
	while ((e = get_env(NULL))->screen == 1)
	{
		if (e->old != e->screen)
		{
			e->old = e->screen;
			str = sec_to_min(time);
			ft_putstr(ft_strjoinf("\rtime: ", str, 2));
		}
		sleep(1);
		if ((e = get_env(NULL))->screen == 1)
		{
			time++;
			str = sec_to_min(time);
			ft_putstr(ft_strjoinf("\rtime: ", str, 2));
		}
	}
	pthread_exit(NULL);
}
