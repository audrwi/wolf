/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   functions2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/24 13:53:01 by aboucher          #+#    #+#             */
/*   Updated: 2016/03/05 16:38:56 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void			buttons(t_env *e, const int *tab, char *str, int n)
{
	int			i;
	int			c;
	const int	x[2] = {tab[0] - 16, tab[0] + 10 * (ft_strlen(str) + 1) + 25};
	const int	y[2] = {tab[1] - 1, tab[1] + 50};

	i = -1;
	c = 0xFFFFFF;
	background(e, x, y, 0x112233);
	if (n == -e->selected && e->selected)
		selected_button(e, x, y, 0x1E8BC3);
	while (i++ <= (int)(ft_strlen(str) + 1))
	{
		mlx_string_put(e->mlx, e->win, tab[0] + 10 * i, tab[1], c, "-");
		mlx_string_put(e->mlx, e->win, tab[0] + 10 * i, tab[1] + 25, c, "-");
	}
	if (n == e->active)
		str = ft_strjoin(">| ", ft_strjoin(str, " |"));
	else
		str = ft_strjoin(" | ", ft_strjoin(str, " |"));
	mlx_string_put(e->mlx, e->win, tab[0] - 15, tab[1] + 12, c, str);
	free(str);
}

void			selected_button(t_env *e, const int *x, const int *y, int color)
{
	const int	pos_x[2] = {x[0] + 16, x[0] + 56};
	const int	pos_y[2] = {y[0] + 12, y[1] - 12};

	background(e, pos_x, pos_y, color);
}

void			background(t_env *e, const int *x, const int *y, int color)
{
	int			x1;
	int			y1;

	x1 = x[0];
	while (x1 < x[1])
	{
		y1 = y[0];
		while (y1 < y[1])
		{
			mlx_pixel_put(e->mlx, e->win, x1, y1, color);
			y1++;
		}
		x1++;
	}
}

void			legend(t_env *e)
{
	const int	bg_x[2] = {WIDTH, WIN_WIDTH};
	const int	bg_y[2] = {0, WIN_HEIGHT};
	const int	first_btn[2] = {WIDTH + 20, 235};
	const int	second_btn[2] = {WIDTH + 80, 235};
	const int	third_btn[2] = {WIDTH + 140, 235};

	background(e, bg_x, bg_y, 0x112233);
	print_motion_keys(e);
	mlx_string_put(e->mlx, e->win, WIDTH + 35, 200, 0xFFFFFF, "Switch to map");
	buttons(e, first_btn, "1", -2);
	buttons(e, second_btn, "2", -3);
	buttons(e, third_btn, "3", -4);
	mini_map(e);
}

void			print_motion_keys(t_env *e)
{
	const int	btn_top[2] = {WIDTH + 80, 60};
	const int	btn_left[2] = {WIDTH + 20, 100};
	const int	btn_bottom[2] = {WIDTH + 80, 100};
	const int	btn_right[2] = {WIDTH + 140, 100};

	mlx_string_put(e->mlx, e->win, WIDTH + 50, 25, 0xFFFFFF, "Motion keys");
	buttons(e, btn_top, "^", -1);
	buttons(e, btn_left, "<", -1);
	buttons(e, btn_bottom, "v", -1);
	buttons(e, btn_right, ">", -1);
}
