/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   screens.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/22 18:38:46 by aboucher          #+#    #+#             */
/*   Updated: 2016/03/06 19:31:29 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void			screen_home(t_env *e)
{
	const int	bg_x[2] = {0, WIN_WIDTH};
	const int	bg_y[2] = {0, WIN_HEIGHT};
	const int	first_btn[2] = {400, 450};
	const int	second_btn[2] = {400, 495};

	if (e->old != e->screen)
	{
		background(e, bg_x, bg_y, 0x112233);
		print_xpm(e, "imgs/cena.xpm", WIN_WIDTH / 2 - 147, 100);
	}
	buttons(e, first_btn, "NEW GAME", 0);
	buttons(e, second_btn, "QUIT", 1);
}

void			screen_play(t_env *e)
{
	pthread_t	thread;

	get_env(e);
	e->img = mlx_new_image(e->mlx, WIDTH, HEIGHT);
	e->data = mlx_get_data_addr(e->img, &e->bpp, &e->size, &e->endian);
	if (e->old != e->screen)
	{
		if (!e->old)
		{
			position_to_center(e);
			e->dir[0] = 1;
			e->dir[1] = 0;
			e->plane[0] = 0;
			e->plane[1] = 0.66;
			e->selected = 2;
			ft_putstr("time: 0:00");
			e->old_time = clock();
		}
		legend(e);
		pthread_create(&thread, NULL, timer, NULL);
	}
	game(e);
	mlx_put_image_to_window(e->mlx, e->win, e->img, 0, 0);
}

void			screen_quit(t_env *e)
{
	const int	bg_x[2] = {300, 600};
	const int	bg_y1[2] = {200, 255};
	const int	bg_y2[2] = {255, 400};
	const int	first_btn[2] = {415, 280};
	const int	second_btn[2] = {415, 325};

	get_env(e);
	if (e->old != e->screen)
	{
		background(e, bg_x, bg_y1, 0x1E8BC3);
		background(e, bg_x, bg_y2, 0x112233);
		mlx_string_put(e->mlx, e->win, 380, 215, 0xFFFFFF, "Quit the game ?");
	}
	buttons(e, first_btn, "YES", 1);
	buttons(e, second_btn, "NO", 0);
}
