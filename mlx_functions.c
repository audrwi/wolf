/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   functions.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/22 12:36:27 by aboucher          #+#    #+#             */
/*   Updated: 2016/03/07 11:05:09 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void			pixel_put_to_image(int color, t_env *e, int x, int y)
{
	char		*color2;

	color = mlx_get_color_value(e->mlx, color);
	color2 = (char *)&color;
	if (x >= 0 && y >= 0 && x < WIDTH && y < HEIGHT)
	{
		e->data[y * e->size + x * e->bpp / 8] = color2[0];
		e->data[y * e->size + x * e->bpp / 8 + 1] = color2[1];
		e->data[y * e->size + x * e->bpp / 8 + 2] = color2[2];
		e->data[y * e->size + x * e->bpp / 8 + 3] = color2[3];
	}
}

void			new_image(t_env *e)
{
	mlx_destroy_image(e->mlx, e->img);
	e->img = mlx_new_image(e->mlx, WIDTH, HEIGHT);
	e->data = mlx_get_data_addr(e->img, &e->bpp, &e->size, &e->endian);
	game(e);
	mlx_put_image_to_window(e->mlx, e->win, e->img, 0, 0);
}

void			print_xpm(t_env *e, char *xpm, int x, int y)
{
	int			w;
	int			h;

	e->img = mlx_xpm_file_to_image(e->mlx, xpm, &w, &h);
	mlx_put_image_to_window(e->mlx, e->win, e->img, x, y);
}

int				close_window(t_env *e)
{
	mlx_destroy_image(e->mlx, e->img);
	mlx_destroy_window(e->mlx, e->win);
	if (e->screen)
		ft_putchar('\n');
	free(e->maps[0]);
	free(e->maps[1]);
	free(e->maps[2]);
	exit(0);
	return (0);
}
