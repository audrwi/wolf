/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/22 11:58:33 by aboucher          #+#    #+#             */
/*   Updated: 2016/03/07 11:53:39 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF_H
# define WOLF_H

# include "libft/libft.h"
# include <mlx.h>
# include <stdio.h>
# include <time.h>
# include <math.h>
# include <pthread.h>

# define WIDTH 700.0
# define HEIGHT 700.0
# define WIN_WIDTH 900
# define WIN_HEIGHT 700

# define MAP_1 "maps/first.map"
# define MAP_2 "maps/second.map"
# define MAP_3 "maps/third.map"

typedef struct		s_map
{
	int				**m;
	int				x;
	int				y;
}					t_map;

typedef struct		s_env
{
	void			*mlx;
	void			*win;
	void			*img;
	char			*data;
	int				bpp;
	int				size;
	int				endian;
	int				old;
	int				screen;
	void			(*tab[3])(struct s_env *e);
	void			(*key[4])(struct s_env *e);
	int				active;
	int				selected;
	int				map;
	t_map			*maps[3];
	clock_t			old_time;
	clock_t			time;
	double			frame_time;
	double			move_speed;
	double			rot_speed;
	double			pos[2];
	double			old_dir;
	double			dir[2];
	double			old_plane;
	double			plane[2];
	double			camera;
	double			ray_pos[2];
	double			ray_dir[2];
	int				map_x;
	int				map_y;
	double			side_dist[2];
	double			delta_dist[2];
	double			wall_dist;
	int				step[2];
	int				hit;
	int				side;
	int				line_height;
	int				draw_start;
	int				draw_end;
}					t_env;

void				pixel_put_to_image(int color, t_env *e, int x, int y);
void				new_image(t_env *e);
void				print_xpm(t_env *e, char *xpm, int x, int y);
int					close_window(t_env *e);

void				buttons(t_env *e, const int *tab, char *str, int n);
void				selected_button(t_env *e, const int *x, const int *y,
					int color);
void				background(t_env *e, const int *x, const int *y, int color);
void				legend(t_env *e);
void				print_motion_keys(t_env *e);

t_map				*fill_map(int fd1, int fd2);
int					*fill_line(int map_y, char **tab);
int					check_map(t_map *map);
int					**malloc_map(int fd);

t_env				*get_env(t_env *e);
char				*sec_to_min(int n);
void				*timer(void *arg);

t_env				*position_to_center(t_env *e);
void				mini_map(t_env *e);
void				draw_position(t_env *e);
void				draw_lines(t_env *e, int x, int y, int rat);
void				change_map(int keycode, t_env *e);

void				screen_home(t_env *e);
void				screen_play(t_env *e);
void				screen_quit(t_env *e);

void				game(t_env *e);
void				vertical_line(t_env *e, int x);

int					key_events(int k, t_env *e);
void				key_right(t_env *e);
void				key_left(t_env *e);
void				key_bottom(t_env *e);
void				key_top(t_env *e);

#endif
