/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/22 12:36:36 by aboucher          #+#    #+#             */
/*   Updated: 2016/03/07 11:59:04 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static t_env	add_functions(t_env e)
{
	e.old = -1;
	e.screen = 0;
	e.active = 0;
	e.map = 0;
	e.tab[0] = screen_home;
	e.tab[1] = screen_play;
	e.tab[2] = screen_quit;
	e.key[0] = key_left;
	e.key[1] = key_right;
	e.key[2] = key_bottom;
	e.key[3] = key_top;
	e.tab[e.screen](&e);
	return (e);
}

int				main(void)
{
	int			i;
	int			fd[6];
	t_env		e;

	i = -1;
	if ((fd[0] = open(MAP_1, O_RDONLY)) == -1 ||
			(fd[1] = open(MAP_2, O_RDONLY)) == -1 ||
			(fd[2] = open(MAP_3, O_RDONLY)) == -1 ||
			(fd[3] = open(MAP_1, O_RDONLY)) == -1 ||
			(fd[4] = open(MAP_2, O_RDONLY)) == -1 ||
			(fd[5] = open(MAP_3, O_RDONLY)) == -1)
		return (ft_puterror("at least a map is missing"));
	while (i++ < 2)
	{
		if (!(e.maps[i] = fill_map(fd[i], fd[i + 3])))
			return (ft_puterror("at least a map is invalid"));
	}
	e.mlx = mlx_init();
	e.win = mlx_new_window(e.mlx, WIN_WIDTH, WIN_HEIGHT, "Cena's Wolf3D");
	e = add_functions(e);
	mlx_hook(e.win, 17, 1, close_window, &e);
	mlx_hook(e.win, 2, 1, key_events, &e);
	mlx_loop(e.mlx);
	return (0);
}
