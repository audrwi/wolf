/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_functions.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/03 16:28:26 by aboucher          #+#    #+#             */
/*   Updated: 2016/03/07 11:36:18 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

t_env			*position_to_center(t_env *e)
{
	int			i;

	i = 0;
	e->pos[0] = (int)(e->maps[e->map]->x / 2);
	e->pos[1] = (int)(e->maps[e->map]->y / 2);
	while (e->maps[e->map]->m[(int)e->pos[0]][(int)e->pos[1]] == 1)
	{
		if (i % 2)
			e->pos[0] *= 1.01;
		else
			e->pos[1] *= 1.01;
		i++;
	}
	e->pos[0] = (int)e->pos[0];
	e->pos[1] = (int)e->pos[1];
	return (e);
}

void			mini_map(t_env *e)
{
	int			x;
	int			y;
	int			left;
	int			c;
	int			rat;

	rat = (WIN_WIDTH - WIDTH - 40) / e->maps[e->map]->x;
	left = WIDTH + 32;
	c = 0xFFFFFF;
	x = 0;
	while (x < e->maps[e->map]->x)
	{
		y = 0;
		while (y < e->maps[e->map]->y)
		{
			if (e->maps[e->map]->m[x][y] > 0)
			{
				mlx_pixel_put(e->mlx, e->win, left + x * rat, 350 + y * rat, c);
				draw_lines(e, x, y, rat);
			}
			y++;
		}
		x++;
	}
	draw_position(e);
}

void			draw_position(t_env *e)
{
	int			x;
	int			y;
	int			rat;

	rat = (WIN_WIDTH - WIDTH - 40) / e->maps[e->map]->x;
	x = WIDTH + 32 + e->pos[0] * rat;
	y = 350 + e->pos[1] * rat;
	mlx_pixel_put(e->mlx, e->win, x, y, 0xFF0000);
}

void			draw_lines(t_env *e, int x, int y, int rat)
{
	int			x_start;
	int			x_end;
	int			y_start;

	if (x - 1 >= 0 && e->maps[e->map]->m[x - 1][y] > 0)
	{
		x_start = WIDTH + 32 + (x - 1) * rat + 1;
		x_end = WIDTH + 32 + x * rat;
		while (x_start < x_end)
		{
			mlx_pixel_put(e->mlx, e->win, x_start, 350 + y * rat, 0xFFFFFF);
			x_start++;
		}
	}
	if (y - 1 >= 0 && e->maps[e->map]->m[x][y - 1])
	{
		x_start = WIDTH + 32 + x * rat;
		y_start = 350 + (y - 1) * rat + 1;
		while (y_start < 350 + y * rat)
		{
			mlx_pixel_put(e->mlx, e->win, x_start, y_start, 0xFFFFFF);
			y_start++;
		}
	}
}

void			change_map(int keycode, t_env *e)
{
	if (e->selected != keycode - 16 && e->selected != keycode - 81)
	{
		e->selected = keycode > 82 ? keycode - 81 : keycode - 16;
		e->map = e->selected - 2;
		position_to_center(e);
		e->dir[0] = 1;
		e->dir[1] = 0;
		e->plane[0] = 0;
		e->plane[1] = 0.66;
		legend(e);
		e->time = clock();
		new_image(e);
	}
}
