/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 17:25:01 by aboucher          #+#    #+#             */
/*   Updated: 2015/12/06 02:18:47 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	count_chars(long n, int i)
{
	if (n / 10)
		return (count_chars(n / 10, ++i));
	return (i);
}

static char	*print_chars(char *s, long n, int len)
{
	len--;
	if (n / 10)
		s[len] = '0' + n % 10;
	else
		s[len] = '0' + n;
	if (len)
		return (print_chars(s, n / 10, len));
	return (s);
}

char		*ft_itoa(int n)
{
	int		i;
	char	*str;
	long	num;

	num = (long)n;
	if (num < 0)
		i = count_chars(-num, 2);
	else
		i = count_chars(num, 1);
	str = (char *)malloc(sizeof(char) * (i + 1));
	if (str == NULL)
		return (NULL);
	if (num < 0)
	{
		str = print_chars(str, -num, i);
		str[0] = '-';
	}
	else
		str = print_chars(str, num, i);
	str[i] = '\0';
	return (str);
}
