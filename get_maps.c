/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_maps.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/07 11:38:17 by aboucher          #+#    #+#             */
/*   Updated: 2016/03/07 11:54:25 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

int				*fill_line(int map_y, char **tab)
{
	int			count;
	int			y;
	int			*map;

	count = 0;
	y = 0;
	if (!tab || !tab[y])
		return (NULL);
	while (tab[count])
		count++;
	if (count != map_y || !(map = (int *)malloc(sizeof(int) * (count + 1))))
		return (NULL);
	while (y < count)
	{
		if (ft_strcmp(tab[y], "1") && ft_strcmp(tab[y], "0"))
			return (NULL);
		map[y] = ft_atoi(tab[y]);
		y++;
	}
	return (map);
}

t_map			*fill_map(int fd1, int fd2)
{
	int			rslt;
	char		*line;
	t_map		*map;

	line = NULL;
	if (!(map = (t_map *)malloc(sizeof(t_map))))
		return (NULL);
	map->x = 0;
	map->y = 0;
	if (!(map->m = malloc_map(fd2)))
		return (NULL);
	while ((rslt = get_next_line(fd1, &line)) > 0)
	{
		while (!map->x && ft_strsplit(line, ' ')[map->y])
			map->y++;
		if (!map->y ||
				!(map->m[map->x] = fill_line(map->y, ft_strsplit(line, ' '))))
			return (NULL);
		free(line);
		map->x++;
	}
	if (rslt == -1 || !map->m || !map->x || !map->y || !check_map(map))
		return (NULL);
	return (map);
}

int				check_map(t_map *map)
{
	int			x;
	int			y;

	x = 0;
	while (x < map->x)
	{
		y = 0;
		while (y < map->y)
		{
			if ((x == 0 || x == map->x - 1) && map->m[x][y] != 1)
				return (0);
			if ((y == 0 || y == map->y - 1) && map->m[x][y] != 1)
				return (0);
			y++;
		}
		x++;
	}
	return (1);
}

int				**malloc_map(int fd)
{
	int			rslt;
	char		*line;
	int			**map;
	int			count;

	line = NULL;
	count = 0;
	while ((rslt = get_next_line(fd, &line)) > 0)
	{
		free(line);
		count++;
	}
	if (rslt == -1)
		return (NULL);
	if (!(map = (int **)malloc(sizeof(int *) * (count + 1))))
		return (NULL);
	return (map);
}
