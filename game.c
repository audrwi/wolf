/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/24 14:54:48 by aboucher          #+#    #+#             */
/*   Updated: 2016/03/07 11:14:14 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static t_env	*norminator_one(t_env *e)
{
	e->ray_pos[0] = e->pos[0];
	e->ray_pos[1] = e->pos[1];
	e->ray_dir[0] = e->dir[0] + e->plane[0] * e->camera;
	e->ray_dir[1] = e->dir[1] + e->plane[1] * e->camera;
	e->map_x = (int)e->ray_pos[0];
	e->map_y = (int)e->ray_pos[1];
	e->delta_dist[0] = e->ray_dir[1] * e->ray_dir[1];
	e->delta_dist[0] /= e->ray_dir[0] * e->ray_dir[0];
	e->delta_dist[0] = sqrt(1 + e->delta_dist[0]);
	e->delta_dist[1] = e->ray_dir[0] * e->ray_dir[0];
	e->delta_dist[1] /= e->ray_dir[1] * e->ray_dir[1];
	e->delta_dist[1] = sqrt(1 + e->delta_dist[1]);
	e->hit = 0;
	return (e);
}

static t_env	*norminator_two(t_env *e)
{
	e->step[0] = e->ray_dir[0] < 0 ? -1 : 1;
	if (e->ray_dir[0] < 0)
		e->side_dist[0] = (e->ray_pos[0] - e->map_x) * e->delta_dist[0];
	else
		e->side_dist[0] = (e->map_x + 1 - e->ray_pos[0]) * e->delta_dist[0];
	e->step[1] = e->ray_dir[1] < 0 ? -1 : 1;
	if (e->ray_dir[1] < 0)
		e->side_dist[1] = (e->ray_pos[1] - e->map_y) * e->delta_dist[1];
	else
		e->side_dist[1] = (e->map_y + 1 - e->ray_pos[1]) * e->delta_dist[1];
	return (e);
}

static t_env	*norminator_three(t_env *e)
{
	while (!e->hit)
	{
		if (e->side_dist[0] < e->side_dist[1])
		{
			e->side_dist[0] += e->delta_dist[0];
			e->map_x += e->step[0];
			e->side = e->ray_dir[0] < 0 ? -1 : 0;
		}
		else
		{
			e->side_dist[1] += e->delta_dist[1];
			e->map_y += e->step[1];
			e->side = e->ray_dir[1] < 0 ? 2 : 1;
		}
		if (e->maps[e->map]->m[e->map_x][e->map_y] > 0)
			e->hit = 1;
	}
	if (e->side < 1)
		e->wall_dist = (e->map_x - e->ray_pos[0] + (1 - e->step[0]) / 2);
	else
		e->wall_dist = (e->map_y - e->ray_pos[1] + (1 - e->step[1]) / 2);
	e->wall_dist /= e->side < 1 ? e->ray_dir[0] : e->ray_dir[1];
	e->line_height = (int)(HEIGHT / e->wall_dist);
	e->draw_start = -e->line_height / 2 + HEIGHT / 2;
	return (e);
}

void			game(t_env *e)
{
	int			x;

	x = -1;
	while (x++ < WIDTH)
	{
		e->camera = 2 * x / WIDTH - 1;
		norminator_three(norminator_two(norminator_one(e)));
		e->draw_start = e->draw_start < 0 ? 0 : e->draw_start;
		e->draw_end = e->line_height / 2 + HEIGHT / 2;
		e->draw_end = e->draw_end >= HEIGHT ? HEIGHT - 1 : e->draw_end;
		if (e->maps[e->map]->m[e->map_x][e->map_y] != 0)
			vertical_line(e, x);
	}
	e->old_time = e->time;
	e->time = clock();
	e->frame_time = (e->time - e->old_time) / 1000;
	e->move_speed = e->frame_time / 20.0;
	e->rot_speed = e->frame_time / 30.0;
}

void			vertical_line(t_env *e, int x)
{
	int			y;
	int			color;
	int			color2;

	color = 0xF64747;
	if (e->map == 1)
		color *= 1.9;
	else if (e->map == 2)
		color *= 0.1;
	if (e->side == 1)
		color2 = color * 1.2;
	else if (e->side == -1)
		color2 = color * 1.4;
	else if (e->side == 2)
		color2 = color * 1.6;
	else
		color2 = color;
	y = e->draw_start - 1;
	while (y++ < e->draw_end)
		pixel_put_to_image(color2, e, x, y);
	y--;
	while (y++ < HEIGHT)
		pixel_put_to_image(color * 1.8, e, x, y);
}
