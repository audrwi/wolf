/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_events.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/22 12:36:03 by aboucher          #+#    #+#             */
/*   Updated: 2016/03/06 19:30:33 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

int				key_events(int k, t_env *e)
{
	e->old = e->screen;
	if ((k == 53 && e->screen != 1) ||
			(e->screen != 1 && e->active == 1 && (k == 36 || k == 76)))
		return (close_window(e));
	else if (k == 53)
	{
		e->screen = 2;
		e->active = 0;
	}
	else if (e->screen != 1 && (k == 125 || k == 126))
		e->active = !e->active;
	else if (e->screen != 1 && (k == 36 || k == 76))
	{
		e->screen = 1;
		mlx_clear_window(e->mlx, e->win);
	}
	if (e->screen == 1 && k > 122 && k < 127)
		e->key[k - 123](e);
	else
		e->tab[e->screen](e);
	if ((k > 17 && k < 21) || (k > 82 && k < 86))
		change_map(k, e);
	return (0);
}

void			key_left(t_env *e)
{
	double		cosinus;
	double		sinus;

	cosinus = cos(-e->rot_speed);
	sinus = sin(-e->rot_speed);
	e->old_dir = e->dir[0];
	e->dir[0] = e->dir[0] * cosinus - e->dir[1] * sinus;
	e->dir[1] = e->old_dir * sinus + e->dir[1] * cosinus;
	e->old_plane = e->plane[0];
	e->plane[0] = e->plane[0] * cosinus - e->plane[1] * sinus;
	e->plane[1] = e->old_plane * sinus + e->plane[1] * cosinus;
	draw_position(e);
	new_image(e);
}

void			key_right(t_env *e)
{
	double		cosinus;
	double		sinus;

	cosinus = cos(e->rot_speed);
	sinus = sin(e->rot_speed);
	e->old_dir = e->dir[0];
	e->dir[0] = e->dir[0] * cosinus - e->dir[1] * sinus;
	e->dir[1] = e->old_dir * sinus + e->dir[1] * cosinus;
	e->old_plane = e->plane[0];
	e->plane[0] = e->plane[0] * cosinus - e->plane[1] * sinus;
	e->plane[1] = e->old_plane * sinus + e->plane[1] * cosinus;
	draw_position(e);
	new_image(e);
}

void			key_bottom(t_env *e)
{
	int			tmp_x;
	int			tmp_y;

	tmp_x = (int)(e->pos[0] - e->dir[0] * e->move_speed);
	tmp_y = (int)(e->pos[1] - e->dir[1] * e->move_speed);
	if (tmp_x > 0 && tmp_x < e->maps[e->map]->x - 1 && (int)e->pos[1] > 0 &&
			(int)e->pos[1] < e->maps[e->map]->x - 1 &&
			!e->maps[e->map]->m[tmp_x][(int)e->pos[1]])
		e->pos[0] -= e->dir[0] * e->move_speed;
	if (tmp_y > 0 && tmp_y < e->maps[e->map]->y - 1 && (int)e->pos[0] > 0 &&
			(int)e->pos[0] < e->maps[e->map]->x - 1 &&
			!e->maps[e->map]->m[(int)e->pos[0]][tmp_y])
		e->pos[1] -= e->dir[1] * e->move_speed;
	draw_position(e);
	new_image(e);
}

void			key_top(t_env *e)
{
	int			tmp_x;
	int			tmp_y;

	tmp_x = (int)(e->pos[0] + e->dir[0] * e->move_speed);
	tmp_y = (int)(e->pos[1] + e->dir[1] * e->move_speed);
	if (tmp_x > 0 && tmp_x < e->maps[e->map]->x - 1 && (int)e->pos[1] > 0 &&
			(int)e->pos[1] < e->maps[e->map]->y - 1 &&
			!e->maps[e->map]->m[tmp_x][(int)e->pos[1]])
		e->pos[0] += e->dir[0] * e->move_speed;
	if (tmp_y > 0 && tmp_y < e->maps[e->map]->y - 1 && (int)e->pos[0] > 0 &&
			(int)e->pos[0] < e->maps[e->map]->x - 1 &&
			!e->maps[e->map]->m[(int)e->pos[0]][tmp_y])
		e->pos[1] += e->dir[1] * e->move_speed;
	draw_position(e);
	new_image(e);
}
