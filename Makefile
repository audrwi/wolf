# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: aboucher <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/02/22 11:59:57 by aboucher          #+#    #+#              #
#    Updated: 2016/03/07 11:37:47 by aboucher         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = wolf3d

SRC = main.c			\
	  get_maps.c		\
	  mlx_functions.c	\
	  draw_functions.c	\
	  time_functions.c	\
	  map_functions.c	\
	  screens.c			\
	  key_events.c		\
	  game.c

OBJ = $(SRC:.c=.o)

HEAD = wolf.h

FLAGS = -Wall -Werror -Wextra -Ofast

MLX = -L/usr/local/lib/ -I/usr/local/include	\
	  -lmlx -framework OpenGL -framework AppKit

all: $(NAME)

$(NAME): $(OBJ) makelibft
	@clang $(OBJ) libft/libft.a $(MLX) -lpthread -o $(NAME)
	@echo "\rCompilating project...	\033[0;32m[OK]\033[0m"
	@echo "\033[0;36m"
	@echo "@@@  @@@  @@@   @@@@@@   @@@       @@@@@@@@  @@@@@@   @@@@@@@ "
	@echo "@@@  @@@  @@@  @@@@@@@@  @@@       @@@@@@@@  @@@@@@@  @@@@@@@@"
	@echo "@@!  @@!  @@!  @@!  @@@  @@!       @@!           @@@  @@!  @@@"
	@echo "!@!  !@!  !@!  !@!  @!@  !@!       !@!           @!@  !@!  @!@"
	@echo "@!!  !!@  @!@  @!@  !@!  @!!       @!!!:!    @!@!!@   @!@  !@!"
	@echo "!@!  !!!  !@!  !@!  !!!  !!!       !!!!!:    !!@!@!   !@!  !!!"
	@echo "!!:  !!:  !!:  !!:  !!!  !!:       !!:           !!:  !!:  !!!"
	@echo ":!:  :!:  :!:  :!:  !:!  :!:       :!:           :!:  :!:  !:!"
	@echo " :::: :: :::   ::::: ::  :: :::::  ::        :: ::::  :::: ::"
	@echo "  :: :  : :     : :  :   : :: : :   :         : : :   :: :  : "
	@echo "\033[0m"

%.o: %.c $(HEAD)
	@clang $(FLAGS) -o $@ -c $< -I$(HEAD)

makelibft:
	@make -j -C libft/

clean:
	@echo "Cleaning objects...	\c"
	@rm -f $(OBJ)
	@make -C libft/ clean
	@echo "\rCleaning objects...	\033[0;32m[OK]\033[0m"

fclean: clean
	@echo "Cleaning target...	\c"
	@rm -f $(NAME)
	@make -C libft/ fclean
	@echo "\rCleaning target...	\033[0;32m[OK]\033[0m"

re: fclean all

.PHONY: all makelibft clean fclean re
